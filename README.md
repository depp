<!--
SPDX-FileCopyrightText: 2025 Vasilij Schneidermann <mail@vasilij.de>

SPDX-License-Identifier: GPL-3.0-or-later
-->

## About

Static Git hosting tool.  Because hosting repositories shouldn't
require a full-blown web application.  Current feature set:

- Initializing repositories and setting up hooks
- Obtaining and setting repository descriptions
- Rendering raw files for the latest revision of the repository
- Rendering the file listing and README for each directory, with special
  support for Markdown
- Generating tarballs for each tagged commit

Everything else is done by the Git/SSH/HTTP configuration of my
server. The tools [git-depp] and [git-export] serve to fill in some
blanks, such as updating static files, editing repository descriptions
and initializing a new repository.

I haven't gotten around to documenting how exactly it all plays
together, mostly because I'm bad at sysadmin tasks. Reuse this at your
own peril (or better, as inspiration for making your own thing, the
code is at less than ~600 SLOC currently and remarkably simple).

Do not use if you care about:

- Viewing branches/tags
- Syntax-highlighted code view
- Administrating repositories from a web UI
- An automatically generated list of projects

[git-depp]: https://depp.brause.cc/tools/git-depp.scm
[git-export]: https://depp.brause.cc/tools/git-export.scm
